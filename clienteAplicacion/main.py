#!/usr/bin/env python

# Core Library modules
import sys
import json
import logging

# Third party modules
import eel
from hwrt import classify
sys.path.insert(0, 'oakDSystem')
import oakDSystem.mainOAKD as OAKD
logger = logging.getLogger(__name__)


def get_json_result(results, n=10):
    results = [{'probability': 0.65,
                 'whatever': 'bar'},
                {'probability': 0.21,
                 'whatever': 'bar'},
                {'probability': 0.05,
                 'whatever': 'bar'}]
    s = []
    last = -1
    for res in results[: min(len(results), n)]:
        if res["probability"] < last * 0.5 and res["probability"] < 0.05:
            break
        if res["probability"] < 0.01:
            break
        s.append(res)
        last = res["probability"]
    return json.dumps(s)


@eel.expose
def startVideoCamaMonitoring():
    results = "hola"
    OAKD.startDetection()
    return results

@eel.expose
def stopVideoCamaMonitoring():
    OAKD.stopDetection()



@eel.expose
def getFeedBack():
    results = OAKD.getFeedBack()
    print(results)
    return results





eel.init("web")
eel.start("/views/distribution/index.html", jinja_templates = "templates")
