# -*- coding: utf-8 -*-
from scipy import spatial
import cv2
import pickle as pkl
import os
import numpy as np
import math

class Recommendation:
    keypointsMapping = ['Nose', 'Neck', 'Right-Shoulder', 'Right-Elbow', 'Right-Wrist', 'Left-Shoulder', 'Left-Elbow', 'Left-Wrist',\
                        'Right-Hip', 'Right-Knee', 'Right-Ankle', 'Left-Hip', 'Left-Knee', 'Left-Ankle', 'Right-Eye', 'Left-Eye',\
                        'Right-Ear', 'Left-Ear']
    
    def __init__(self, therapist_dir=r"oakDSystem\therapist_template"):
        self.therapist = os.getcwd()+os.sep+therapist_dir
        all_files = os.listdir(self.therapist)
        self.allposes = []
        for e in all_files:
            if ".dat" in e:
                self.allposes.append(e)
                
    def read_poses(self,dir_):
        data = None
        with open(dir_, "rb") as f:
            data = pkl.load(f)
        return data
    
    def L2_norm(self,d):
        d_transf = []
        for k in range(0,18):	
            if len(d[1][k])>0:
                v = d[1][k][0][0:2]
                v = v/np.linalg.norm(v)
                d_transf.append((v[0],v[1],d[1][k][0][2],d[1][k][0][3]))
            else:
                d_transf.append(d[1][k])
        return d_transf
            
    def scoring_distances(self,df1, df2):
        result = {}
        for i in range(0,18):
            if (len(df1[i])>0) & (len(df2[i])>0):
                result.update({self.keypointsMapping[i]: (1-spatial.distance.cosine(df1[i][0:2],df2[i][0:2]), np.mean([df1[i][2],df2[i][2]]))})
        return result        
    
    def recommendation(self,result):
        
        left = (None,np.Inf)
        right = (None,np.Inf)
        
        for i in range(0,18):
            if self.keypointsMapping[i] in result.keys():
                if "Left" in self.keypointsMapping[i]:
                    if result[self.keypointsMapping[i]][0]<left[1]:
                        left=(self.keypointsMapping[i].split("-")[1],result[self.keypointsMapping[i]][0])
                if "Right" in self.keypointsMapping[i]:
                     if result[self.keypointsMapping[i]][0]<right[1]:
                        right=(self.keypointsMapping[i].split("-")[1],result[self.keypointsMapping[i]][0])
        
        if left[1]<right[1]:
            angle = math.acos(right[1])
            ang_deg = math.degrees(angle)%360
            if ang_deg > 10:
                return "Left "+left[0]+ " is disaligned "+ str(int(ang_deg)) + " degrees."
            else: 
                return "Good job"
        else:
            angle = math.acos(right[1])
            ang_deg = math.degrees(angle)%360
            if ang_deg > 10:
                return "Right "+right[0]+ " is disaligned "+ str(int(ang_deg)) + " degrees."
            else:
                return "Good job"
    
    def master(self, cam_pose):
        therap_pose = self.allposes[0]
        img2 = None
        rec = None
        
        dir__ = self.therapist+os.sep+therap_pose
        ## read pkl and images
        data_1 = cam_pose
        data_2 = self.read_poses(dir__)
        try:
            d1_transf = self.L2_norm(data_1)
        except:
            pass
        else:
            d2_transf = self.L2_norm(data_2)
             
            result = self.scoring_distances(d1_transf,d2_transf)   
            
            img2 = cv2.imread(self.therapist+os.sep+therap_pose.split(".")[0]+".jpg")
            
            rec = self.recommendation(result)
                                 
        finally:
            return img2, rec