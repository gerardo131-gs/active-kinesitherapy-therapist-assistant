import time
from pathlib import Path
from humanPoseEstimation.humanPoseEstimation2D.pose import getKeypoints, getValidPairs, getPersonwiseKeypoints
import cv2
import depthai as dai
import numpy as np
import base64
from measureTrainingEffectiveness.matricDTW2D.compare_poses import Recommendation
import threading


debug = True
recommendation = Recommendation()
colors = [[0, 100, 255], [0, 100, 255], [0, 255, 255], [0, 100, 255], [0, 255, 255], [0, 100, 255], [0, 255, 0],
          [255, 200, 100], [255, 0, 255], [0, 255, 0], [255, 200, 100], [255, 0, 255], [0, 0, 255], [255, 0, 0],
          [200, 200, 0], [255, 0, 0], [200, 200, 0], [0, 0, 0]]
POSE_PAIRS = [[1, 2], [1, 5], [2, 3], [3, 4], [5, 6], [6, 7], [1, 8], [8, 9], [9, 10], [1, 11], [11, 12], [12, 13],
              [1, 0], [0, 14], [14, 16], [0, 15], [15, 17], [2, 17], [5, 16]]
running = True
keypoints_list = None
detected_keypoints = None
personwiseKeypoints = None
DetectionIsRun = True
width = None
height = None
textResult = {"text": "Kinematic body not detecting", "image64Base": ""}


class FPSHandler:
    def __init__(self, cap=None):
        self.timestamp = time.time()
        self.start = time.time()
        self.framerate = cap.get(cv2.CAP_PROP_FPS) if cap is not None else None

        self.frame_cnt = 0
        self.ticks = {}
        self.ticks_cnt = {}

    def next_iter(self):
        if not True:
            frame_delay = 1.0 / self.framerate
            # frame_delay = 1.0 / 3*self.framerate
            delay = (self.timestamp + frame_delay) - time.time()
            if delay > 0:
                time.sleep(delay)
        self.timestamp = time.time()
        self.frame_cnt += 1

    def tick(self, name):
        if name in self.ticks:
            self.ticks_cnt[name] += 1
        else:
            self.ticks[name] = time.time()
            self.ticks_cnt[name] = 0

    def tick_fps(self, name):
        if name in self.ticks:
            return self.ticks_cnt[name] / (time.time() - self.ticks[name])
        else:
            return 0

    def fps(self):
        return self.frame_cnt / (self.timestamp - self.start)


if True:
    fps = FPSHandler()
else:
    cap = cv2.VideoCapture(str(Path(None).resolve().absolute()))
    fps = FPSHandler(cap)


def cos_dist(a, b):
    return np.dot(a, b) / (np.linalg.norm(a) * np.linalg.norm(b))


def to_tensor_result(packet):
    return {
        tensor.name: np.array(packet.getLayerFp16(tensor.name)).reshape(tensor.dims)
        for tensor in packet.getRaw().tensors
    }


def frame_norm(frame, bbox):
    return (np.clip(np.array(bbox), 0, 1) * np.array([*frame.shape[:2], *frame.shape[:2]])[::-1]).astype(int)


def to_planar(arr: np.ndarray, shape: tuple) -> list:
    return cv2.resize(arr, shape).transpose(2, 0, 1).flatten()


def create_pipeline():
    # print("Creating pipeline...")
    pipeline = dai.Pipeline()
    cam = None
    if True:
        # ColorCamera
        # print("Creating Color True...")
        cam = pipeline.createColorCamera()
        cam.setPreviewSize(456, 256)
        cam.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1080_P)
        cam.setInterleaved(False)
        cam.setBoardSocket(dai.CameraBoardSocket.RGB)
        cam_xout = pipeline.createXLinkOut()
        cam_xout.setStreamName("cam_out")
        cam.preview.link(cam_xout.input)
        controlIn = pipeline.createXLinkIn()
        controlIn.setStreamName('control')
        controlIn.out.link(cam.inputControl)

    # NeuralNetwork
    # print("Creating Human Pose Estimation Neural Network...")
    pose_nn = pipeline.createNeuralNetwork()
    if True:
        pose_nn.setBlobPath(
            str(Path("oakDSystem/models/human-pose-estimation-0001_openvino_2021.2_6shave.blob").resolve().absolute()))
    else:
        pose_nn.setBlobPath(
            str(Path("oakDSystem/models/human-pose-estimation-0001_openvino_2021.2_8shave.blob").resolve().absolute()))
    # Increase threads for detection
    pose_nn.setNumInferenceThreads(2)
    # Specify that network takes latest arriving frame in non-blocking manner
    pose_nn.input.setQueueSize(1)
    pose_nn.input.setBlocking(False)
    pose_nn_xout = pipeline.createXLinkOut()
    pose_nn_xout.setStreamName("pose_nn")
    pose_nn.out.link(pose_nn_xout.input)

    if True:
        cam.preview.link(pose_nn.input)
    else:
        pose_in = pipeline.createXLinkIn()
        pose_in.setStreamName("pose_in")
        pose_in.out.link(pose_nn.input)

    # print("Pipeline created.")
    return pipeline


def pose_thread(in_queue):
    global keypoints_list, detected_keypoints, personwiseKeypoints
    global height
    global width
    global running
    global fps

    while running:
        try:
            raw_in = in_queue.get()
        except RuntimeError:
            return
        fps.tick('nn')
        heatmaps = np.array(raw_in.getLayerFp16('Mconv7_stage2_L2')).reshape((1, 19, 32, 57))
        pafs = np.array(raw_in.getLayerFp16('Mconv7_stage2_L1')).reshape((1, 38, 32, 57))
        heatmaps = heatmaps.astype('float32')
        pafs = pafs.astype('float32')
        outputs = np.concatenate((heatmaps, pafs), axis=1)

        new_keypoints = []
        new_keypoints_list = np.zeros((0, 3))
        keypoint_id = 0

        for row in range(18):
            probMap = outputs[0, row, :, :]
            probMap = cv2.resize(probMap, (width, height))  # (456, 256)
            keypoints = getKeypoints(probMap, 0.3)
            new_keypoints_list = np.vstack([new_keypoints_list, *keypoints])
            keypoints_with_id = []

            for i in range(len(keypoints)):
                keypoints_with_id.append(keypoints[i] + (keypoint_id,))
                keypoint_id += 1

            new_keypoints.append(keypoints_with_id)

        valid_pairs, invalid_pairs = getValidPairs(outputs, width, height, new_keypoints)
        newPersonwiseKeypoints = getPersonwiseKeypoints(valid_pairs, invalid_pairs, new_keypoints_list)

        detected_keypoints, keypoints_list, personwiseKeypoints = (
        new_keypoints, new_keypoints_list, newPersonwiseKeypoints)


def worker():
    global debug
    global running
    global recommendation
    global colors
    global POSE_PAIRS
    global width
    global height
    global keypoints_list
    global detected_keypoints
    global personwiseKeypoints
    global cap
    global textResult
    global DetectionIsRun

    with dai.Device(create_pipeline()) as device:
        # print("Starting pipeline...")
        device.startPipeline()
        if True:
            cam_out = device.getOutputQueue("cam_out", 1, True)
            controlQueue = device.getInputQueue('control')
        else:
            pose_in = device.getInputQueue("pose_in")
        pose_nn = device.getOutputQueue("pose_nn", 1, False)
        t = threading.Thread(target=pose_thread, args=(pose_nn,))
        t.start()

        def get_frame():
            if None:
                return cap.read()
            else:
                return True, np.array(cam_out.get().getData()).reshape((3, 256, 456)).transpose(1, 2, 0).astype(
                    np.uint8)

        try:
            while DetectionIsRun:
                read_correctly, frame = get_frame()
                if not read_correctly:
                    break
                fps.next_iter()
                height, width = frame.shape[:2]  # 256, 456
                debug_frame = frame.copy()
                if not True:
                    nn_data = dai.NNData()
                    nn_data.setLayer("input", to_planar(frame, (456, 256)))
                    pose_in.send(nn_data)
                if debug:
                    """
                    if keypoints_list is not None and detected_keypoints is not None and personwiseKeypoints is not None:
                        for i in range(18):
                            for j in range(len(detected_keypoints[i])):
                                cv2.circle(debug_frame, detected_keypoints[i][j][0:2], 5, colors[i], -1, cv2.LINE_AA)
                        for i in range(17):
                            for n in range(len(personwiseKeypoints)):
                                try:
                                    index = personwiseKeypoints[n][np.array(POSE_PAIRS[i])]
                                except:
                                    pass
                                else:
                                    if -1 in index:
                                        continue
                                    B = np.int32(keypoints_list[index.astype(int), 0])
                                    A = np.int32(keypoints_list[index.astype(int), 1])
                                    cv2.line(debug_frame, (B[0], A[0]), (B[1], A[1]), colors[i], 3, cv2.LINE_AA)
                    """
                    therapist, rec = recommendation.master([keypoints_list, detected_keypoints, personwiseKeypoints])
                    winname = "rgb"
                    cv2.namedWindow(winname)  # Create a named window
                    cv2.moveWindow(winname, 40, 30)  # Move it to (40,30)
                    im_h = None
                    if therapist is not None:
                        im_h = cv2.hconcat([debug_frame, therapist])
                        if rec is not None:
                            print(rec)

                            if "Good" in rec:
                                recommendation.allposes.remove(recommendation.allposes[0])
                                cv2.waitKey(200)
                                if len(recommendation.allposes) == 0:
                                    break

                            textResult["text"] = rec
                        jpg_as_text = base64.b64encode(cv2.imencode('.jpg', im_h)[1]).decode()
                        textResult["image64Base"] = jpg_as_text
                        # cv2.waitKey(0)
                        #cv2.imshow(winname, im_h)
                    else:
                        # Create a black image
                        image = np.zeros((height, width, 3), np.uint8)
                        im_h = cv2.hconcat([debug_frame, image])
                        #cv2.imshow(winname, im_h)
                        textResult["text"] = "Kinematic body not detecting"
                        jpg_as_text = base64.b64encode(cv2.imencode('.jpg', im_h)[1]).decode()
                        textResult["image64Base"] = jpg_as_text
                key = cv2.waitKey(1)
                if key == ord('q'):
                    break

                elif key == ord('t'):
                    # print("Autofocus trigger (and disable continuous)")
                    ctrl = dai.CameraControl()
                    ctrl.setAutoFocusMode(dai.CameraControl.AutoFocusMode.AUTO)
                    ctrl.setAutoFocusTrigger()
                    controlQueue.send(ctrl)
        except Exception as e:
            print(e)
            raise
        running = False
    if not True:
        cap.release()
    textResult = {"text": "Well done", "image64Base": ""}


def startDetection():
    global recommendation
    global running
    global keypoints_list
    global detected_keypoints
    global personwiseKeypoints
    global DetectionIsRun
    global width
    global height
    global textResult
    global fps
    global cap
    threading.Thread(target=worker, daemon=True).start()
    recommendation = Recommendation()
    running = True
    keypoints_list = None
    detected_keypoints = None
    personwiseKeypoints = None
    DetectionIsRun = True
    width = None
    height = None
    textResult = {"text": "Kinematic body not detecting", "image64Base": ""}
    if True:
        fps = FPSHandler()
    else:
        cap = cv2.VideoCapture(str(Path(None).resolve().absolute()))
        fps = FPSHandler(cap)


def stopDetection():
    global DetectionIsRun
    DetectionIsRun = False


def getFeedBack():
    global textResult
    result = textResult
    return result